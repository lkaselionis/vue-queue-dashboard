import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from "./views/Dashboard";
import Administrator from "./views/Administrator";
import Specialist from "./views/Specialist";
import Visitor from "./views/Visitor";

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard
        },
        {
            path: '/administrator',
            name: 'administrator',
            component: Administrator
        },
        {
            path: '/specialist',
            name: 'specialist',
            component: Specialist
        },
        {
            path: '/visitor',
            name: 'visitor',
            component: Visitor
        },
    ]
})