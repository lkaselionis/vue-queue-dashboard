class Services {
    constructor() {
        this.list = [];
        this.lastId = 0;

        this.add('Main service', 200, 205);
    }

    add(name, rangeStart, rangeEnd) {
        this.lastId++;

        // todo - mising range validity check
        // if range is invalid, trow exception

        this.list.push({
            id: this.lastId,
            name: name,
            rangeStart: rangeStart,
            rangeEnd: rangeEnd,
            lastId: null
        });
    }

    get(serviceId) {
        let [service] = this.list.filter(item => item.id === serviceId);
        return service;    
    }

    getRange(serviceId) {
        let service = this.get(serviceId);
        return {
            start: service.rangeStart,
            end: service.rangeEnd
        };
    }

}

module.exports = new Services();